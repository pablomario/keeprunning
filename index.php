<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<link rel="icon" href="img/favicon.png" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="./css/estilo.css">
	</head>
	<body>	
		<?php include('./core/nav/nav.php'); ?>
	
		<div class="centrado">
			<section id="apuntate" class="carreras">
				<h1>Apúntate ahora</h1>
				<div class="alCentro">				
					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>
				</div>
			</section>
				

			<aside>
				<section>
					<article>
						<h1>Despues de una dura carrera</h1>
						<img src="./img/spam/mrm.png">
					</article>
				</section>
				
			</aside>


			<section class="carreras grande">
				<h1>Descubre nuevas carreras</h1>
					<div class="alCentro">	
					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>				

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>

					<article>
						<img src="./img/races/default.png" alt="imagen">
						<h2>Nombre de una Carrera</h2>
						<p>Descripcion corta de la carrera y algo mas pero tampoco mucho</p>
					</article>
				</div>
			</section>			
		</div>
		

		<footer>
			
		</footer>
	</body>
</html>