<header>
	<div class="centrado">
		<img id="logotipo" src="./img/logo-mini.png">
		<nav class="navbar_top">				
							
		</nav>
	</div>
	<nav class="navbar_bottom">
		<div class="centrado">
			<ul>
				<a href="#" class="active"><li>inicio</li></a>
				<a href="#"><li>Proximos eventos</li></a>
				<a href="#"><li>Descubre</li></a>				
				<a href="#" class="nuevo_usuario"><li>Nuevo Usuario</li></a>
				<a href="#" class="organizadores"><li>Organizadores</li></a>
				<a href="#"><li>Casos de Exito</li></a>	
				<a href="#"><li>Contacto</li></a>				
			</ul>
		</div>		
	</nav>
	<div class="centrado">
		<div id="jumbotron">		
			<article>
				<h1>Nunca pares de Correr</h1>
				<p>Está prohibido rendirse, respira hondo y sigue.</p>
			</article>		
		</div>
	</div>
</header>